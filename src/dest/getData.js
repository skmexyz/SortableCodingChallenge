'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.products = exports.listings = undefined;

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _fs2 = require('fs');

var _fs3 = _interopRequireDefault(_fs2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _fs = _bluebird2.default.promisifyAll(_fs3.default);

var listings = exports.listings = undefined,
    products = exports.products = undefined;

/**
 * This will get the strings from the file and return usuable objects from it
 * @param location location of the file
 */
function getUsableData(location) {
    return new _bluebird2.default(function (resolve, reject) {
        _fs.readFileAsync(location, 'utf-8').then(function (contents) {
            var array = contents.split(/\n/);
            for (var i = 0; i < array.length - 1; i++) {
                array[i] = JSON.parse(array[i]);
            }
            resolve(array);
        }).catch(function (err) {
            reject(err);
        });
    });
}

exports.listings = listings = getUsableData('../files/listings.txt');
exports.products = products = getUsableData('../files/products.txt');