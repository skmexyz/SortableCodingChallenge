'use strict';

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _getData = require('./getData');

var _processData = require('./processData');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * This is the main function
 */
_bluebird2.default.all([_getData.products, _getData.listings]).then(function (data) {
    var products = data[0].slice(0, data[0].length - 1);
    var listings = data[1].slice(0, data[1].length - 1);
    return (0, _processData.finalResult)(products, listings);
}).then(function (results) {
    var result = "";
    for (var i = 0; i < results.length; i++) {
        result = result + JSON.stringify(results[i]) + '\n';
    }
    _fs2.default.writeFile('../files/results.txt', result, function (error) {
        if (!error) console.log('all done! :)');else console.log(error);
    });
}).catch(function (error) {
    console.log(error);
});