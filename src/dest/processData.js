"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.finalResult = finalResult;

var _bluebird = require("bluebird");

var _bluebird2 = _interopRequireDefault(_bluebird);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * This will create an array containing two arrays
 * (one contains manufacturers of products, and the other contains the products itself)
 * @param products - the objects from products.txt
 * @returns {*[]} - [manufacturers, products] - products itself is an array containing products sorted by manufacturers
 */
function manufacturersAndProducts(products) {
    var manufacturers = [],
        _products = [];
    for (var i = 0; i < products.length; i++) {
        var manufacturer = products[i].manufacturer;
        if (manufacturers.includes(manufacturer)) {
            var productsByManufacturer = _products[manufacturers.indexOf(manufacturer)];
            productsByManufacturer.push(products[i]);
            _products[manufacturers.indexOf(manufacturer)] = productsByManufacturer;
        } else {
            manufacturers.push(manufacturer);
            _products[manufacturers.length - 1] = [products[i]];
        }
    }
    return [manufacturers, _products];
}

/**
 * Sort the listings based on manufacturers from products.txt
 * @param listings - the objects from listings.txt
 * @param manufacturers - the manufacturers array containing manufacturers from products.txt
 * @returns {Array} - of sorted listings based on manufacturers (from products.txt)
 */
function sortListings(listings, manufacturers) {
    var sortedListings = [];
    for (var i = 0; i < manufacturers.length; i++) {
        sortedListings[i] = [];
        var listingsForProduct = [];
        for (var j = 0; j < listings.length; j++) {
            var listingName = listings[j].title;
            if (listingName.includes(manufacturers[i])) {
                listingsForProduct = sortedListings[i];
                listingsForProduct.push(listings[j]);
                sortedListings[i] = listingsForProduct;
            }
        }
    }
    return sortedListings;
}

/**
 * This will generate an array of regex given a product array from a manufacturer
 * @param productsOfManufacturer an array of products by the manufacturer
 * @returns {Array} an array of regex for those products
 */
function getRegex(productsOfManufacturer) {
    var _searchTerms = [];
    for (var i = 0; i < productsOfManufacturer.length; i++) {
        var model = productsOfManufacturer[i].model;
        var searchTerms = permutations(model);
        _searchTerms.push(searchTerms);
    }
    var _tooCommonTerms = tooCommonTerms(_searchTerms);
    for (var i = 0; i < _searchTerms.length; i++) {
        var productTerms = _searchTerms[i];
        var _productTerms = [];
        for (var j = 0; j < productTerms.length; j++) {
            var counter = 0;
            for (var k = 0; k < _tooCommonTerms.length; k++) {
                if (productTerms[j].toLowerCase() == _tooCommonTerms[k].toLowerCase()) {
                    counter++;
                }
            }
            if (counter == 0) _productTerms.push(productTerms[j]);
        }
        _searchTerms[i] = _productTerms;
    }
    for (var i = 0; i < _searchTerms.length; i++) {
        var regex = "";
        for (var j = 0; j < _searchTerms[i].length; j++) {
            regex = regex + "|(" + _searchTerms[i][j] + ")";
        }
        _searchTerms[i] = regex.substr(1, regex.length);
    }
    return _searchTerms;
}

/**
 * Return an array of common search terms for a product family (help reduce false positives)
 * @param searchTerms Array of search terms of a manufacturer
 * @returns {Array} Array of common search terms
 */
function tooCommonTerms(searchTerms) {
    var array1 = [],
        array2 = [];
    for (var i = 0; i < searchTerms.length; i++) {
        for (var j = 0; j < searchTerms[i].length; j++) {
            if (array1.indexOf(searchTerms[i][j]) > -1) {
                array2[array1.indexOf(searchTerms[i][j])] += 1;
            } else {
                array1.push(searchTerms[i][j]);
                array2[array1.length - 1] = 1;
            }
        }
    }
    var commonRegex = [];
    for (var i = 0; i < array1.length; i++) {
        if (array2[i] >= 2) commonRegex.push(array1[i]);
    }
    return commonRegex;
}

/**
 * Will generate permutations of the search terms given a specific model
 * @param term The product model
 * @returns {Array} Array consisting of search terms for that specific model
 */
function permutations(term) {
    var _permutations = [];
    if (term.includes('-')) {
        var firstpart = term.substr(0, term.indexOf('-'));
        var lastpart = term.substr(term.indexOf('-') + 1, term.length);
        _permutations.push(term);
        _permutations.push(firstpart);
        _permutations.push(lastpart);
        _permutations.push(firstpart + lastpart);
    } else _permutations.push(term);
    if (term.substr(term.length - 1, term.length).charCodeAt(0) >= 65) {
        _permutations.push(term.substr(0, term.length - 1));
        var __permutations = permutations(term.substr(0, term.length - 1));
        _permutations = _permutations.concat(__permutations);
    }
    for (var i = 0; i < _permutations.length; i++) {
        if (/^\d+$/.test(_permutations[i])) _permutations.splice(i, 1);
    }
    return _permutations;
}

/**
 * Will renerate the final necessary object
 * @param listings Listings of the specific manufacturer
 * @param product product object
 * @param regex regex for the project
 * @returns {{}} final object as requested
 */
function ListingsForProduct(listings, product, regex) {
    if (regex == undefined || product == undefined || listings == undefined) reject(listings);
    var listingsForProduct = [];
    for (var i = 0; i < listings.length; i++) {
        var listingTitle = listings[i].title;
        if (regex.length > 0) var model = new RegExp(regex).test(listingTitle);else {
            var obj = {};
            obj.product_name = product.product_name;
            obj.listings = [];
            return obj;
        }
        var manufacturer = new RegExp(product.manufacturer).test(listingTitle);
        if (model && manufacturer) listingsForProduct.push(listings[i]);
    }
    var obj = {};
    obj.product_name = product.product_name;
    obj.listings = listingsForProduct;
    return obj;
}

/**
 * Will use the above functions to create the necessary result object array and return it as a promise
 * @param products listings of products from products.txt
 * @param listings listings of listings from listings.txt
 */
function finalResult(products, listings) {
    return new _bluebird2.default(function (resolve, reject) {
        var _manufacturersAndProducts = manufacturersAndProducts(products);
        var manufacturers = _manufacturersAndProducts[0];
        var sortedProducts = _manufacturersAndProducts[1];
        var sortedListings = sortListings(listings, manufacturers);
        var RegexArray = [];
        for (var i = 0; i < sortedProducts.length; i++) {
            RegexArray.push(getRegex(sortedProducts[i]));
        }
        var results = [];
        for (var i = 0; i < sortedProducts.length; i++) {
            for (var j = 0; j < RegexArray[i].length; j++) {
                results.push(ListingsForProduct(sortedListings[i], sortedProducts[i][j], RegexArray[i][j]));
            }
        }
        resolve(results);
    });
}