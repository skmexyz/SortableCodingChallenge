import gulp from 'gulp';
import babel from 'gulp-babel';

gulp.task('babelify', () => {
    return gulp.src('src/**/*.js')
        .pipe(babel())
        .pipe(gulp.dest('dest/'));
});

gulp.task('default', ['babelify']);