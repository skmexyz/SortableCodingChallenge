import Promise from 'bluebird';
import fs from 'fs';
import {products, listings} from './getData';
import {finalResult} from './processData';


/**
 * This is the main function
 */
Promise.all([products, listings])
    .then(function (data) {
        var products = data[0].slice(0, data[0].length - 1);
        var listings = data[1].slice(0, data[1].length - 1);
        return finalResult(products, listings);
    })
    .then(function (results) {
        var result = "";
        for(var i = 0; i < results.length; i++) {
            result = result + JSON.stringify(results[i]) + '\n';
        }
        fs.writeFile('../files/results.txt', result, function (error) {
            if(!error) console.log('all done! :)');
            else console.log(error);
        })
    })
    .catch(function (error) {
        console.log(error);
    })