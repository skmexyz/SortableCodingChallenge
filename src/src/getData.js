import Promise from 'bluebird';
import fs from 'fs';
var _fs = Promise.promisifyAll(fs);

export var listings, products;

/**
 * This will get the strings from the file and return usuable objects from it
 * @param location location of the file
 */
function getUsableData(location) {
    return new Promise(function (resolve, reject) {
        _fs.readFileAsync(location, 'utf-8')
            .then(function (contents) {
                var array = contents.split(/\n/);
                for(var i = 0; i < array.length - 1; i++) {
                    array[i] = JSON.parse(array[i]);
                }
                resolve(array);
            })
            .catch(function (err) {
                reject(err);
            })
    });
}

listings = getUsableData('../files/listings.txt');
products = getUsableData('../files/products.txt')