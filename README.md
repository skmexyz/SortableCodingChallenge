# Sortable Coding Challenge

## [Question available here](http://sortable.com/challenge/)

## Instructions to run the code

1) Download and install node and npm
2) Clone the repo
3) run `cd src && npm install`
4) After installing dependencies, run `gulp`
5) After files have been generated in `./dest`, run `cd dest && node sortData.js`

## Improvements
1) This skips products (around 5 out of the total 743) as the product model has common model numbers (compared to rest of the models in the same family)
2) Take family name into account wherever possible
3) Convert to English (not really necessary but might help)